clear all
close all


NS=5;


nsite{1}=2; B{1}=2.0; text13{2}='gray'; meth{2}='Euler'; text14{2}='Euler';
nsite{2}=2; B{2}=2.0; text13{1}='gray'; meth{1}='rk4'; text14{1}='rk4';


xmin=0; xmax=1.2;



Ndat=max(size(B));
for js=1:NS
    for jj=1:Ndat

        if (js==1)
            stem{jj}=sprintf('pt0B0B2.0S.5_%s_%s/results.h5',text13{jj},meth{jj});
        else
            stem{jj}=sprintf('pt0B0B2.0S%1.1f_%s_%s/results.h5',js*0.5,text13{jj},meth{jj});
        end

        fold{jj}=sprintf('pu'); 
        S{jj}(js)=js*0.5;
        Bt{jj,js} = h5read(sprintf('%s',stem{jj}),sprintf('/%s/Bt',fold{jj}));
        Mz{jj,js} = h5read(sprintf('%s',stem{jj}),sprintf('/%s/Mz',fold{jj}));
        Mzref{jj,js} = h5read(sprintf('%s',stem{jj}),sprintf('/%s/Mzref',fold{jj}));
        energy{jj,js} = h5read(sprintf('%s',stem{jj}),sprintf('/%s/e',fold{jj}));
        fidelity{jj,js} = h5read(sprintf('%s',stem{jj}),sprintf('/%s/fidelity',fold{jj}));
        ngates{jj,js} = h5read(sprintf('%s',stem{jj}),sprintf('/%s/ngates',fold{jj}));
        t{jj,js} = h5read(sprintf('%s',stem{jj}),sprintf('/%s/t',fold{jj}));
        tsteps{jj}(js)=max(size(t{jj,js}));

        Nthet0_1{jj}(js)=ngates{jj,js}(1,1);
        Nthet0_2{jj}(js)=ngates{jj,js}(2,1);

        Nthetc_1{jj}(js)=ngates{jj,js}(1,end);
        Nthetc_2{jj}(js)=ngates{jj,js}(2,end);

    end
end


nIDs = 2;
alphabet = ('a':'z').';
chars = num2cell(alphabet(1:nIDs));
chars = chars.';
charlbl = strcat('(',chars,')'); % {'(a)','(b)','(c)','(d)'}


fig0=figure(1306);
clf;

nfont=14;
markS=10;

lsty{1}='-ko';
lsty{2}='-rs';

markF{1}='k';
markF{2}='r';

 

subplot(2,1,1)
for jj=1:Ndat
    xx=S{jj}; yy=Nthetc_2{jj}*2;
    plot(xx,yy,lsty{jj},'linewidth',1,'MarkerSize',markS,'MarkerFaceColor',markF{jj})
    if (jj==1)
        hold on
        max1=max(double(yy)); min1=min(double(yy));
    else
        max2=max(double(yy)); min2=min(double(yy));
        max1=max(max1,max2); min1=min(min1,min2);
    end
    leg{jj}=sprintf('%s',text14{jj});
end

xlimit=[min(xx)-0.05*(max(xx)-min(xx)),max(xx)+0.05*(max(xx)-min(xx))];
ylimit=[min1-0.1*(max1-min1),max1+0.1*(max1-min1)];

xlim(xlimit)
ylim(ylimit)

xlabel('$s$','interpreter','latex')
ylabel('# CNOTs')

legend(leg,'location','southeast')
legend boxoff


text(0.025,0.85,charlbl{1},'Units','normalized','FontSize',12)
set(gca,'linewidth',2,'FontSize',nfont,'layer','top','FontName','Times New Roman')





subplot(2,1,2)
xx=S{jj}; yy=tsteps{2}./tsteps{1};
plot(xx,yy,'bd-','linewidth',1,'MarkerSize',markS,'MarkerFaceColor','b')
max1=max(double(yy)); min1=min(double(yy));


xlimit=[min(xx)-0.05*(max(xx)-min(xx)),max(xx)+0.05*(max(xx)-min(xx))];
ylimit=[min1-0.1*(max1-min1),max1+0.1*(max1-min1)];

xlim(xlimit)
ylim(ylimit)

xlabel('$s$','interpreter','latex')
ylabel(sprintf('%s','ratio'))



text(0.025,0.85,charlbl{2},'Units','normalized','FontSize',12)
set(gca,'linewidth',2,'FontSize',nfont,'layer','top','FontName','Times New Roman')


        