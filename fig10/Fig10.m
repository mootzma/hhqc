clear all
close all
clc;


S{1}=25; nsite{1}=2;  B{1}=40.0;
S{2}=2.5; nsite{2}=2;  B{2}=4.0;

% text14{1}='QM'; fold0{1}='c1';
% text14{2}='MF'; fold0{2}='mf3';

text14{1}='QM'; fold0{1}='c2';
text14{2}='MF'; fold0{2}='mf4';



Ndat=max(size(S));
tau{1}=1:0.1:20.0;
tau{2}=1:0.1:20.0;

for jj=1:Ndat
    Ntau{jj}=max(size(tau{jj}));
end

tmin{1}=25.; tmax{1}=49;
tau_min{1}=3.5; tau_max{1}=18.5;

tmin{2}=25; tmax{2}=49;
tau_min{2}=3.5; tau_max{2}=18.5;


ymin=-0.66;   ymax=0.9;

xmax2=2.4;
ymin2=1e-4;


tau2pl{1}=1.2; 
tau2pl{2}=1.2; 

% read in energy levels of original H
fold00=sprintf('S5n2');  
h5file0='results_exp.h5';
ene0 = h5read(sprintf('%s',h5file0),sprintf('/%s/ene',fold00));
dip0 = h5read(sprintf('%s',h5file0),sprintf('/%s/dipole',fold00));


%%%%%%%%%%%%%%%%
% exp ratios
h=load('ratios_exp.dat');
xe=h(:,1);
ye=h(:,2);
%%%%%%%%%%%%%%%%





% read in pump-probe data
for jj=1:Ndat
    for jt=1:Ntau{jj}

        if (jj==1)
            h5file{jj}=sprintf('%st%1.1fB%1.1fB%1.1fS%1.1f_std_rk4/results.h5',fold0{jj},tau{jj}(jt)*10,B{jj},B{jj},S{jj});
        else
            h5file{jj}=sprintf('%st%1.1fB%1.1fB%1.1fS%1.1f_std_rk4/results.h5',fold0{jj},tau{jj}(jt),B{jj},B{jj},S{jj});
        end
        if (jt==1)
            tpro{jj} = h5read(sprintf('%s',h5file{jj}),sprintf('/pro/t'));
            Bpro{jj} = h5read(sprintf('%s',h5file{jj}),sprintf('/pro/Bt'));

            Mproref{jj} = h5read(sprintf('%s',h5file{jj}),sprintf('/pro/Mz'));
        end

        
    
        tpp{jj,jt} = h5read(sprintf('%s',h5file{jj}),sprintf('/pp/t'));
        Bpp{jj,jt} = h5read(sprintf('%s',h5file{jj}),sprintf('/pp/Bt'));

        ene{jj,jt} = h5read(sprintf('%s',h5file{jj}),sprintf('/pp/ene'));

        gap{jj}=ene{jj}(2)-ene{jj}(1);
        if (jj==1)
            wAF{jj}=gap{jj}/2/pi;
        else
            wAF{jj}=gap{jj}/pi;
        end
   
    
    
        Mppref{jj,jt} = h5read(sprintf('%s',h5file{jj}),sprintf('/pp/Mz'));
        Mpp2ref{jj,jt} = h5read(sprintf('%s',h5file{jj}),sprintf('/pp/Mz'));

        tpu{jj,jt} = h5read(sprintf('%s',h5file{jj}),sprintf('/pu/t'));

    
        Mpuref{jj,jt} = h5read(sprintf('%s',h5file{jj}),sprintf('/pu/Mz'));
    end
    


end

for jj=1:Ndat
    tint{jj}=tpp{jj,1};  
end

% construct nonlinear response
for jj=1:Ndat
    for jt=1:Ntau{jj} 
        Mnlref{jj}(:,jt)=Mppref{jj,jt}(:)-Mpuref{jj,jt}(:)-Mproref{jj};
        Mnlref{jj}(:,jt)=Mnlref{jj}(:,jt)-Mnlref{jj}(1,jt); 
    end



    nn=max(size(tpu{jj}));
    Tf=tmax{jj}-tmin{jj};
    for jt=1:nn
        if (tpu{jj}(jt)>tmin{jj} && tpu{jj}(jt)<tmax{jj})
            WB{jj}(jt)=0.42-0.5*cos(2*pi*(tpu{jj}(jt)-tmin{jj})/Tf)+0.08*cos(2*pi*(tpu{jj}(jt)-tmin{jj})/Tf);
        else
            WB{jj}(jt)=0;
        end
    end
    Tf=tau_max{jj}-tau_min{jj};
    for jt=1:Ntau{jj}
        if (tau{jj}(jt)>tau_min{jj} && tau{jj}(jt)<tau_max{jj})
            WB_tau{jj}(jt)=0.42-0.5*cos(2*pi*(tau{jj}(jt)-tau_min{jj})/Tf)+0.08*cos(2*pi*(tau{jj}(jt)-tau_min{jj})/Tf);
        else
            WB_tau{jj}(jt)=0;
        end
    end

    filt{jj}=WB{jj}'.*WB_tau{jj};


    Mnl_ftref_filt{jj}=abs(fft2(Mnlref{jj}.*filt{jj}));
    Mnl_ftref_filt{jj}=fftshift(Mnl_ftref_filt{jj});


    for jt=1:Ntau{jj}
        Mnl_sl{jj}(:,jt)=abs(fft2((Mnlref{jj}(:,jt)-Mnlref{jj}(1,jt)).*WB{jj}'));
        Mnl_sl{jj}(:,jt)=fftshift(Mnl_sl{jj}(:,jt));
    end
end


for jj=1:Ndat
    Nt{jj}=max(size(tint{jj}));
    delta_t{jj}=tint{jj}(2)-tint{jj}(1);
    omega_t{jj}=(-Nt{jj}/2:Nt{jj}/2-1)/(Nt{jj}*delta_t{jj}); 

    delta_del=(tau{jj}(2)-tau{jj}(1));
    omega_tau{jj}=(-Ntau{jj}/2:Ntau{jj}/2-1)/(Ntau{jj}*delta_del);
end





% determine strength of HHG
N2pl=max(size(tau2pl));
nHHG=5;
for jj=1:Ndat
    for jt=1:N2pl
        i1=find(tau{jj}<=tau2pl{jt}/wAF{1},1,'last');
        yy=Mnl_sl{jj}(:,i1);
        dw=0.025;
        xx=omega_t{jj};
        for jn=0:nHHG
            x1=wAF{jj}*jn-dw; x2=wAF{jj}*jn+dw;
            in1=find(xx <= x1,1,'last');
            in2=find(xx <= x2,1,'last');
            mHH{jt,jj}(jn+1)=max(yy(in1:in2));
        end
    end
end







nfont=16;
map11 =[
    1.0000    1.0000    1.0000
    0.9751    0.9751    0.9834
    0.9501    0.9501    0.9668
    0.9252    0.9252    0.9502
    0.9003    0.9003    0.9336
    0.8753    0.8753    0.9170
    0.8504    0.8504    0.9004
    0.8255    0.8255    0.8838
    0.8005    0.8005    0.8672
    0.7756    0.7756    0.8506
    0.7507    0.7507    0.8340
    0.7257    0.7257    0.8174
    0.7008    0.7008    0.8008
    0.6758    0.6758    0.7842
    0.6509    0.6509    0.7676
    0.6260    0.6260    0.7510
    0.6010    0.6010    0.7344
    0.5761    0.5761    0.7178
    0.5512    0.5512    0.7012
    0.5262    0.5262    0.6846
    0.5013    0.5013    0.6680
    0.4764    0.4764    0.6514
    0.4514    0.4514    0.6348
    0.4265    0.4265    0.6182
    0.4016    0.4016    0.6016
    0.3944    0.3944    0.6087
    0.3872    0.3872    0.6158
    0.3801    0.3801    0.6229
    0.3729    0.3729    0.6300
    0.3657    0.3657    0.6371
    0.3585    0.3585    0.6443
    0.3514    0.3514    0.6514
    0.3442    0.3442    0.6585
    0.3370    0.3370    0.6656
    0.3299    0.3299    0.6727
    0.3227    0.3227    0.6798
    0.3155    0.3155    0.6869
    0.3083    0.3083    0.6941
    0.3012    0.3012    0.7012
    0.2940    0.2940    0.7083
    0.2868    0.2868    0.7154
    0.2797    0.2797    0.7225
    0.2725    0.2725    0.7296
    0.2653    0.2653    0.7368
    0.2582    0.2582    0.7439
    0.2510    0.2510    0.7510
    0.2438    0.2438    0.7581
    0.2366    0.2366    0.7652
    0.2295    0.2295    0.7723
    0.2223    0.2223    0.7794
    0.2151    0.2151    0.7866
    0.2080    0.2080    0.7937
    0.2008    0.2008    0.8008
    0.1936    0.1936    0.8079
    0.1864    0.1864    0.8150
    0.1793    0.1793    0.8221
    0.1721    0.1721    0.8292
    0.1649    0.1649    0.8364
    0.1578    0.1578    0.8435
    0.1506    0.1506    0.8506
    0.1434    0.1434    0.8577
    0.1362    0.1362    0.8648
    0.1291    0.1291    0.8719
    0.1219    0.1219    0.8790
    0.1147    0.1147    0.8862
    0.1076    0.1076    0.8933
    0.1004    0.1004    0.9004
    0.0932    0.0932    0.9075
    0.0861    0.0861    0.9146
    0.0789    0.0789    0.9217
    0.0717    0.0717    0.9289
    0.0645    0.0645    0.9360
    0.0574    0.0574    0.9431
    0.0502    0.0502    0.9502
    0.0430    0.0430    0.9573
    0.0359    0.0359    0.9644
    0.0287    0.0287    0.9715
    0.0215    0.0215    0.9787
    0.0143    0.0143    0.9858
    0.0072    0.0072    0.9929
         0         0    1.0000
    0.0086    0.0001    0.9911
    0.0172    0.0003    0.9821
    0.0258    0.0004    0.9732
    0.0344    0.0006    0.9643
    0.0430    0.0007    0.9553
    0.0516    0.0008    0.9464
    0.0603    0.0010    0.9375
    0.0689    0.0011    0.9286
    0.0775    0.0013    0.9196
    0.0861    0.0014    0.9107
    0.0947    0.0015    0.9018
    0.1033    0.0017    0.8928
    0.1119    0.0018    0.8839
    0.1205    0.0020    0.8750
    0.1291    0.0021    0.8660
    0.1377    0.0022    0.8571
    0.1463    0.0024    0.8482
    0.1549    0.0025    0.8393
    0.1636    0.0027    0.8303
    0.1722    0.0028    0.8214
    0.1808    0.0029    0.8125
    0.1894    0.0031    0.8035
    0.1980    0.0032    0.7946
    0.2066    0.0034    0.7857
    0.2152    0.0035    0.7767
    0.2238    0.0036    0.7678
    0.2324    0.0038    0.7589
    0.2410    0.0039    0.7499
    0.2496    0.0041    0.7410
    0.2582    0.0042    0.7321
    0.2669    0.0043    0.7232
    0.2755    0.0045    0.7142
    0.2841    0.0046    0.7053
    0.2927    0.0048    0.6964
    0.3013    0.0049    0.6874
    0.3099    0.0050    0.6785
    0.3185    0.0052    0.6696
    0.3271    0.0053    0.6606
    0.3357    0.0055    0.6517
    0.3443    0.0056    0.6428
    0.3529    0.0057    0.6338
    0.3615    0.0059    0.6249
    0.3702    0.0060    0.6160
    0.3788    0.0062    0.6071
    0.3874    0.0063    0.5981
    0.3960    0.0064    0.5892
    0.4046    0.0066    0.5803
    0.4132    0.0067    0.5713
    0.4218    0.0069    0.5624
    0.4304    0.0070    0.5535
    0.4390    0.0071    0.5445
    0.4476    0.0073    0.5356
    0.4562    0.0074    0.5267
    0.4648    0.0076    0.5177
    0.4735    0.0077    0.5088
    0.4821    0.0078    0.4999
    0.4907    0.0080    0.4910
    0.4993    0.0081    0.4820
    0.5079    0.0082    0.4731
    0.5165    0.0084    0.4642
    0.5251    0.0085    0.4552
    0.5337    0.0087    0.4463
    0.5423    0.0088    0.4374
    0.5509    0.0089    0.4284
    0.5595    0.0091    0.4195
    0.5682    0.0092    0.4106
    0.5768    0.0094    0.4017
    0.5854    0.0095    0.3927
    0.5940    0.0096    0.3838
    0.6026    0.0098    0.3749
    0.6112    0.0099    0.3659
    0.6198    0.0101    0.3570
    0.6284    0.0102    0.3481
    0.6370    0.0103    0.3391
    0.6456    0.0105    0.3302
    0.6542    0.0106    0.3213
    0.6628    0.0108    0.3123
    0.6714    0.0109    0.3034
    0.6801    0.0110    0.2945
    0.6887    0.0112    0.2856
    0.6973    0.0113    0.2766
    0.7059    0.0115    0.2677
    0.7145    0.0116    0.2588
    0.7231    0.0117    0.2498
    0.7317    0.0119    0.2409
    0.7403    0.0120    0.2320
    0.7489    0.0122    0.2230
    0.7575    0.0123    0.2141
    0.7661    0.0124    0.2052
    0.7748    0.0126    0.1962
    0.7834    0.0127    0.1873
    0.7920    0.0129    0.1784
    0.8006    0.0130    0.1695
    0.8092    0.0131    0.1605
    0.8178    0.0133    0.1516
    0.8264    0.0134    0.1427
    0.8350    0.0136    0.1337
    0.8436    0.0137    0.1248
    0.8522    0.0138    0.1159
    0.8608    0.0140    0.1069
    0.8694    0.0141    0.0980
    0.8780    0.0143    0.0891
    0.8867    0.0144    0.0802
    0.8953    0.0145    0.0712
    0.9039    0.0147    0.0623
    0.9125    0.0148    0.0534
    0.9211    0.0150    0.0444
    0.9297    0.0151    0.0355
    0.9253    0.0160    0.0377
    0.9209    0.0170    0.0399
    0.9165    0.0179    0.0422
    0.9121    0.0189    0.0444
    0.9077    0.0198    0.0466
    0.9033    0.0208    0.0488
    0.8989    0.0217    0.0510
    0.8945    0.0227    0.0533
    0.8902    0.0236    0.0555
    0.8858    0.0245    0.0577
    0.8814    0.0255    0.0599
    0.8770    0.0264    0.0622
    0.8726    0.0274    0.0644
    0.8682    0.0283    0.0666
    0.8638    0.0293    0.0688
    0.8594    0.0302    0.0710
    0.8550    0.0312    0.0733
    0.8506    0.0321    0.0755
    0.8462    0.0331    0.0777
    0.8418    0.0340    0.0799
    0.8374    0.0349    0.0821
    0.8330    0.0359    0.0844
    0.8286    0.0368    0.0866
    0.8242    0.0378    0.0888
    0.8198    0.0387    0.0910
    0.8155    0.0397    0.0932
    0.8111    0.0406    0.0955
    0.8067    0.0416    0.0977
    0.8023    0.0425    0.0999
    0.7979    0.0434    0.1021
    0.7935    0.0444    0.1043
    0.7891    0.0453    0.1066
    0.7847    0.0463    0.1088
    0.7803    0.0472    0.1110
    0.7759    0.0482    0.1132
    0.7715    0.0491    0.1155
    0.7671    0.0501    0.1177
    0.7627    0.0510    0.1199
    0.7583    0.0519    0.1221
    0.7539    0.0529    0.1243
    0.7495    0.0538    0.1266
    0.7452    0.0548    0.1288
    0.7408    0.0557    0.1310
    0.7364    0.0567    0.1332
    0.7320    0.0576    0.1354
    0.7276    0.0586    0.1377
    0.7232    0.0595    0.1399
    0.7188    0.0604    0.1421
    0.7144    0.0614    0.1443
    0.7100    0.0623    0.1465
    0.7056    0.0633    0.1488
    0.7012    0.0642    0.1510
    0.6968    0.0652    0.1532
    0.6924    0.0661    0.1554
    0.6880    0.0671    0.1576
    0.6836    0.0680    0.1599
    0.6792    0.0690    0.1621
    0.6748    0.0699    0.1643
    0.6705    0.0708    0.1665
    0.6661    0.0718    0.1688
    0.6617    0.0727    0.1710
    0.6573    0.0737    0.1732
    0.6529    0.0746    0.1754
    0.6485    0.0756    0.1776
    0.6441    0.0765    0.1799
    0.6397    0.0775    0.1821
    0.6353    0.0784    0.1843];

nIDs = 8;
alphabet = ('a':'z').';
chars = num2cell(alphabet(1:nIDs));
chars = chars.';
charlbl = strcat('(',chars,')'); 




nfont=16;
fig0=figure(1307);



jj=1;
subplot(1,3,1)
yy=-omega_tau{jj}; xx=omega_t{jj}; zz=Mnl_ftref_filt{jj};
zz=zz'./max(max(abs(zz)));
contourf(xx,yy,zz,123,'EdgeColor','none')


axis equal
caxis([0.0,1])
ylimit=[ymin,ymax];
xlimit=[0,1.05];
xlim(xlimit)
ylim(ylimit)


hold on

colormap(map11);
colorbar('south')


xlabel('$\omega_t (J)$','Interpreter','latex')
ylabel('$\omega_\tau (J)$','Interpreter','latex')

for jn=1:30
    plot(jn*[gap{jj} gap{jj}]/(2*pi),ylimit,'--c')
    plot(xlimit,(jn-15)*[gap{jj} gap{jj}]/(2*pi),'--c')
end


text(0.025,0.95,charlbl{1},'Units','normalized','FontSize',nfont)
set(gca,'linewidth',2,'FontSize',nfont,'FontName','Times New Roman')
set(gcf,'Renderer','Painter')



lsty{1}='-r';
lsty{2}='-b';


lsty2{1}='-ro';
lsty2{2}='-bd';



subplot(1,3,2)
for jj=1:Ndat
    i1=find(tau{jj}<=tau2pl{jj}/wAF{1},1,'last');
    xx=omega_t{jj}/wAF{jj}; yy=Mnl_sl{jj}(:,i1);
    yy=yy./max(yy);
    p44(jj)=plot(xx,yy,lsty{jj},'linewidth',2);
    if (jj==1)
        max1=max(yy); min1=min(yy);
        hold on
    else
        max2=max(yy); min2=min(yy);
        min1=min(min1,min2); max1=max(max1,max2);
    end
    leg2{jj}=sprintf('%s',text14{jj});
end
xmax2=1.2;
xlimit=[0,7.6];
ylimit=[ymin2,1e1];
xlim(xlimit)
ylim(ylimit)


for jn=1:8
    plot(jn*[1 1],[10^-6,1e1],'--k','linewidth',1)
end

legend(p44,leg2,'Location','south')
legend boxoff
xlabel('$\omega_t/\omega_{AF}$','Interpreter','latex')
if (jj==round(Ndat/2)+1 || jj==1)
    ylabel(sprintf('M^z_{NL}(%s, %s)','\omega','\tau'))
end
text(0.025,0.95,charlbl{2},'Units','normalized','FontSize',nfont)
set(gca,'linewidth',2,'FontSize',nfont,'layer','top','yscale','log','FontName','Times New Roman')





for jj=1:Ndat
    for jn=0:nHHG
        r{jj}(jn+1)=mHH{jj,jj}(jn+1)/mHH{jj,jj}(2);
    end
end


mark{1}='o'; mark{2}='d'; mark{3}='x'; mark{4}='d'; mark{5}='x';
col{1}='r'; col{2}='b';

subplot(1,3,3)

plot(xe,ye,'-sk','MarkerSize',12,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor',[.65 .65 .65])
leg3{1}='exp';
hold on
for jj=1:Ndat
    xx=0:1:nHHG; yy=r{jj};
    plot(xx,yy,lsty2{jj},'linewidth',1,'MarkerSize',10,'MarkerFaceColor',col{jj})
    if (jj==1)
        max1=max(yy); min1=min(yy);
        hold on
    else
        max2=max(yy); min2=min(yy);
        min1=min(min1,min2); max1=max(max1,max2);
    end
    leg3{jj+1}=sprintf('%s',text14{jj});
end

xlimit=[min(xx)-0.05*(max(xx)-min(xx)),max(xx)+0.05*(max(xx)-min(xx))];
ylimit=[1e-4,10^0.5];
xlim(xlimit)
ylim(ylimit)

legend(leg3,'Location','southwest')
legend boxoff


ylabel('HHG #n/HHG #1')
xlabel('$n$','Interpreter','latex')


text(0.825,0.95,charlbl{3},'Units','normalized','FontSize',nfont)

set(gca,'linewidth',2,'FontSize',nfont,'layer','top','yscale','log','FontName','Times New Roman')



axes('Position',[.5 .7025 .12 .22],'Units','normalized')
box on
jj=1;
Ndim=max(size(ene{jj}));
% jind{jj}=0:1:Ndim-1;
% yy4=ene{jj}/(2*pi);
% bar(jind{jj},yy4,0.7,'EdgeColor','none')

yy4=ene0/(2*pi);
dim=max(size(yy4));
for k=1:dim
    plot([0,0.5],[yy4(k),yy4(k)],'-k','linewidth',1)
    if k==1
        hold on
    end
end
w=yy4(2)-yy4(1);
% for k=1:17
%     plot([0,0.5],[yy4(1),yy4(1)]+w*k,'--r')
% end
%             bar(jind{jj},yy4,0.7,'EdgeColor','none')

ylimit=[min(yy4)-0.1*(max(yy4)-min(yy4)),max(yy4)+0.1*(max(yy4)-min(yy4))];
ylim(ylimit)
xlim([-0.2,1.5])

%             xlabel('$n$','FontSize',4)
if (jj==1)
    ylabel('$E_n$','FontSize',4)
end
set(gca,'linewidth',2,'xtick',[],'ytick',[-1,0,1])

ylimit=[min(yy4)-0.1*(max(yy4)-min(yy4)),max(yy4)+0.1*(max(yy4)-min(yy4))];
ylim(ylimit)

% xlabel('$n$','FontSize',14,'Interpreter','latex')
ylabel('$E_n$','FontSize',14,'Interpreter','latex')
set(gca,'linewidth',2,'layer','top','FontName','Times New Roman')
