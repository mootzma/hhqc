Data souce and scripts to generate the figures of the following publication:
- Martin Mootz, Peter P Orth, Chuankun Huang, Liang Luo, Jigang Wang, Yong-Xin Yao, Two-dimensional coherent spectrum of high-spin models via a quantum computing approach, arXiv:2311.14035
